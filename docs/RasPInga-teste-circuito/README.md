# RasPInga

Protótipo de solução para controle de nível da caixa d'água utilizando `Raspberry PI 2`, `sensores` e `LEDs`.

```sh

  `.::///+:/-.        --///+//-:``    
 `+oooooooooooo:   `+oooooooooooo:    
  /oooo++//ooooo:  ooooo+//+ooooo.    
  `+ooooooo:-:oo-  +o+::/ooooooo:     
   `:oooooooo+``    `.oooooooo+-      
     `:++ooo/.        :+ooo+/.`       
        ...`  `.----.` ``..           fabio@laborautonomo.org
     .::::-``:::::::::.`-:::-`        -----------------------
    -:::-`   .:::::::-`  `-:::-       OS: Raspbian GNU/Linux 10 (buster) armv6l
   `::.  `.--.`  `` `.---.``.::`      Model: Raspberry Pi Model B Plus Rev 1.2
       .::::::::`  -::::::::` `       Kernel: 4.19.50+
 .::` .:::::::::- `::::::::::``::.
-:::` ::::::::::.  ::::::::::.`:::-   CPU: BCM2835 (1) @ 700MHz                        
::::  -::::::::.   `-::::::::  ::::   Memory: 129MiB / 432MiB
-::-   .-:::-.``....``.-::-.   -::-
 .. ``       .::::::::.     `..`..
   -:::-`   -::::::::::`  .:::::`
   :::::::` -::::::::::` :::::::.
   .:::::::  -::::::::. ::::::::
    `-:::::`   ..--.`   ::::::.
      `...`  `...--..`  `...`
            .::::::::::
             `.-::::-`


```

# Requerimentos

* [Python 3](https://www.python.org/download/releases/3.0/)
* [GPIO](https://sourceforge.net/p/raspberry-gpio-python/wiki/Examples/)
* [gpiozero](https://gpiozero.readthedocs.io/en/stable/index.html)

