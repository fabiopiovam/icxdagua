from gpiozero import LED, Button

leds = [17, 27]
btns = [26, 19]

leds = [LED(pino) for pino in leds]
btns = [Button(pino) for pino in btns]

while True:
    for i, pino in enumerate(leds):
        if not btns[i].is_pressed:
            leds[i].on()
        else:
            leds[i].off()

