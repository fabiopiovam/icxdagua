from gpiozero import LED, Button
from time import sleep

led1 = LED(17)
led2 = LED(27)
led3 = LED(22)

while True:

    led1.on()
    sleep(1)
    led2.on()
    sleep(1)
    led3.on()

    sleep(1)

    led1.off()
    sleep(1)
    led2.off()
    sleep(1)
    led3.off()
    sleep(1)

