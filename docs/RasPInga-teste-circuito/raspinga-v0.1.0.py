import RPi.GPIO as GPIO

leds = [17, 27]
btns = [26, 19]

GPIO.setmode(GPIO.BCM)
GPIO.setup(leds, GPIO.OUT)
GPIO.setup(btns, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    for i, pino in enumerate(leds):
        if GPIO.input(btns[i]):
            GPIO.output(leds[i], 1)
        else:
            GPIO.output(leds[i], 0)

