#define sensor1 D8
#define sensor2 D7
#define sensor3 D6

#define led1 D2
// #define led2 D1
#define led2 D0
// #define led2 D0
#define led3 D1

void setup() {
  Serial.begin(115200);

  iniciarSensores();
}

void loop() {
  String sensores = lerSensores();

  Serial.println(sensores);

  delay(100);
}

void iniciarSensores() {
  int valor_s1 = 0, valor_s2 = 0, valor_s3 = 0;

  //Define os pinos dos sensores
  pinMode(sensor1, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);

  // define os pinos dos les indicadores
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);  
}

String lerSensores() {
  //Leitura dos sensores
  int valor_s1 = digitalRead(sensor1);
  int valor_s2 = digitalRead(sensor2);
  int valor_s3 = digitalRead(sensor3);

  //Mostra valor dos sensores no serial monitor
  Serial.print("S1: ");
  Serial.print(valor_s1);
  Serial.print(" S2: ");
  Serial.print(valor_s2);
  Serial.print(" S3: ");
  Serial.println(valor_s3);

  //Checa o nivel
  if (valor_s1 == 1) {
    digitalWrite(led1, HIGH);
  }
  else {
    digitalWrite(led1, LOW);
  }

  if (valor_s2 == 1) {
    digitalWrite(led2, HIGH);
  }
  else {
    digitalWrite(led2, LOW);
  }

  if (valor_s3 == 1) {
    digitalWrite(led3, HIGH);
  }
  else {
    digitalWrite(led3, LOW);
  }

  return String(valor_s1) + ";" + String(valor_s2) + ";" + String(valor_s3);
}
