am4core.ready(function() {

  // Themes begin
  am4core.useTheme(am4themes_animated);

  // Create chart instance
  var chart = am4core.create("chartdiv-content", am4charts.XYChart3D);

  // TODO Adicionar esta informação à área de config no arquivo .ino
  chart.titles.create().text = "Ed. Gaivotas";
  chart.titles.create().text = "Nível da caixa d'água";

  // Add data
  chart.data = [{
    "category": "",
    "value1": 20,
    "value2": 80
  }];

  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "category";
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.grid.template.strokeOpacity = 0;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.grid.template.strokeOpacity = 0;
  valueAxis.min = -1;
  valueAxis.max = 150;
  valueAxis.strictMinMax = false;
  valueAxis.renderer.baseGrid.disabled = true;
  valueAxis.renderer.labels.template.adapter.add("text", function(text) {
    if ((text > 100) || (text < 0)) {
      return "";
    }
    else {
      return text + "%";
    }
  })

  // Create series
  var series1 = chart.series.push(new am4charts.ConeSeries());
  series1.dataFields.valueY = "value1";
  series1.dataFields.categoryX = "category";
  series1.columns.template.width = am4core.percent(90);
  series1.columns.template.fillOpacity = 0.9;
  series1.columns.template.strokeOpacity = 1;
  series1.columns.template.strokeWidth = 2;

  var series2 = chart.series.push(new am4charts.ConeSeries());
  series2.dataFields.valueY = "value2";
  series2.dataFields.categoryX = "category";
  series2.stacked = true;
  series2.columns.template.width = am4core.percent(90);
  series2.columns.template.fill = am4core.color("#000");
  series2.columns.template.fillOpacity = 0.1;
  series2.columns.template.stroke = am4core.color("#000");
  series2.columns.template.strokeOpacity = 0.2;
  series2.columns.template.strokeWidth = 2;

  var value1 = 20;

  function data_update(data) {
    console.log(data);

    // atualizar status

    // class de #chartdiv-status
    document.getElementById('chartdiv-status').className = data['status_geral'];
    // atualizar texto de #status-nivel
    document.getElementById('status-nivel').innerHTML = data['nivel_status'];
    // atualizar texto de #status-nivel-details
    document.getElementById('status-nivel-details').innerHTML = data['nivel'];
    // atualizar texto de #status-sensores
    document.getElementById('status-sensores').innerHTML = data['sensores_status'];
    // atualizar texto de #status-sensores-details
    document.getElementById('status-sensores-details').innerHTML = data['sensores'];

    // atualizar grafico
    value1 = data['nivel_num'];
    value2 = 100 - value1;

    chart.data[0].value1 = value1;
    chart.data[0].value2 = value2;
    chart.invalidateRawData();
  }

  get_data = function() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        data = JSON.parse(this.responseText);
        data_update(data);
      }
    };
    xhttp.open("GET", "/getData", false);
    xhttp.send();
  }

  get_data();

  // TODO Adicionar este tempo de atualização 
  // à área de config no arquivo .ino
  setInterval(function() {get_data();}, 4000);

}); // end am4core.ready()
