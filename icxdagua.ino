/*
 * ESP8266 SPIFFS HTML Web Page with JPEG, PNG Image 
 * https://circuits4you.com
 */

#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>


// --- CONFIG LEDS & SENSORES

#define sensor1 D8
#define sensor2 D7
#define sensor3 D6

#define led1 D2 // vermelho
#define led2 D1 // verde
#define led3 D0 // amarelo


// --- CONFIG WIFI

const char* ssid = "gaivota bla 2 andar";
const char* password = "08003000";


void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();

  Serial.println("File System Initialized");
  
  conectarWifi();

  iniciarSensores();
}

void loop() {
  enviarDadosSensores();
  
  delay(30000);  //Send a request every 30 seconds
 
}


// --- FUNCOES AUXILIARES

void enviarDadosSensores() {
  if(WiFi.status() == WL_CONNECTED) {
    HTTPClient http; //Declare object of class HTTPClient
  
    http.begin("http://192.168.1.88:8085/hello"); //Specify request destination
    http.addHeader("Content-Type", "text/plain"); //Specify content-type header
  
    String data = lerSensores();
    int httpCode = http.POST(data); //Send the request
    String payload = http.getString(); //Get the response payload
  
    Serial.println(httpCode); //Print HTTP return code
    Serial.println(payload); //Print request response payload
  
    http.end(); //Close connection
  }
  else {
     Serial.println("Error in WiFi connection");   
  }
}

void conectarWifi() {
  //Connect to wifi Network
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
}

void iniciarSensores() {
  int valor_s1 = 0, valor_s2 = 0, valor_s3 = 0;

  //Define os pinos dos sensores
  pinMode(sensor1, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);

  // define os pinos dos les indicadores
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);  
}

String lerSensores() {
  //Leitura dos sensores
  int valor_s1 = digitalRead(sensor1);
  int valor_s2 = digitalRead(sensor2);
  int valor_s3 = digitalRead(sensor3);

  // TODO retirar esta linha abaixo quando o sensor tiver instalado
  valor_s3 = 0;

  String percent_minimo = "25";
  String percent_medio = "65";
  String percent_maximo = "90";

  String json = "";
  String status_geral = "normal";
  String sensores = "";
  String sensores_status = "normal";
  String nivel = "";
  String nivel_status = "";
  int nivel_num = 10;

  //Checa o nivel
  if (valor_s1 == 1) {
    digitalWrite(led1, HIGH);
    nivel_num = 35;
    status_geral = "alerta";
    nivel_status = "abaixo da metade";
    nivel = "O nível de água está entre 20% e 40%";
    sensores = "<li>* - Ligado</li>";
  }
  else {
    digitalWrite(led1, LOW);
    status_geral = "perigo";
    nivel_status = "muito baixo";
    nivel = "O nível de água está abaixo de 20%";
    sensores = "<li>- - Desligado</li>";
  }

  if (valor_s2 == 1) {
    digitalWrite(led2, HIGH);
    nivel_num = 65;
    status_geral = "normal";
    nivel_status = "acima da metade";
    nivel = "O nível de água está entre 60% e 80%";
    sensores = "<li>* - Ligado</li>" + sensores;
    if (valor_s1 == 0) {
      sensores_status = "Há um erro no sensor 1";
      status_geral = "aviso";
    }
  }
  else {
    digitalWrite(led2, LOW);
    sensores = "<li>- - Desligado</li>" + sensores;
  }

  if (valor_s3 == 1) {
    digitalWrite(led3, HIGH);
    nivel_num = 90;
    status_geral = "perigo";
    nivel_status = "muito acima";
    nivel = "O nível de água está acima de " + percent_maximo + "%";
    sensores = "<li>* - Ligado</li>" + sensores;
    if (valor_s1 == 0 && valor_s2 == 0) {
      sensores_status = "Há problemas nos sensores 1 e 2";
    }
    else {
      if (valor_s2 == 0) {
        sensores_status = "Há um erro no sensor 2";
      }
      
      if (valor_s1 == 0) {
        sensores_status = "Há um erro no sensor 1";  
      }
    }
  }
  else {
    digitalWrite(led3, LOW);
    sensores = "<li>- - Desligado</li>" + sensores;
  }

  json = "{\"sensores\": \"" + sensores;
  json = json + "\", \"sensores_status\":\"" + sensores_status;
  json = json + "\", \"status_geral\":\"" + status_geral;
  json = json + "\", \"nivel_num\":" + String(nivel_num);
  json = json + ", \"nivel\":\"" + nivel;
  json = json + "\", \"nivel_status\":\"" + nivel_status + "\"}";

  return json;
}
