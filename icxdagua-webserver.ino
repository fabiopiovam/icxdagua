/*
 * ESP8266 SPIFFS HTML Web Page with JPEG, PNG Image 
 * https://circuits4you.com
 */

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>   //Include File System Headers


// --- CONFIG LEDS & SENSORES

#define sensor1 D8
#define sensor2 D7
#define sensor3 D6

#define led1 D2 // vermelho
#define led2 D1 // verde
#define led3 D0 // amarelo


// --- CONFIG WIFI

const char* ssid = "gaivota bla 2 andar";
const char* password = "08003000";

ESP8266WebServer server(80);


void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();

  //Initialize File System
  SPIFFS.begin();
  Serial.println("File System Initialized");
  
  conectarWifi();

  iniciarSensores();

  //Initialize Webserver
  server.on("/",handleRoot);
  server.on("/getData",handleData); //Reads data function is called from out index.html
  server.onNotFound(handleWebRequests); //Set setver all paths are not found so we can handle as per URI
  server.begin();
}

void loop() {
 server.handleClient();
}


// --- HANDLE PAGES

void handleRoot(){
  server.sendHeader("Location", "/index.html",true);   //Redirect to our html web page
  server.send(302, "text/plane","");
}

void handleData() {
  String sensores = lerSensores();

  Serial.println(sensores);

  server.send(200, "text/plane",sensores);
}

void handleWebRequests(){
  if(loadFromSpiffs(server.uri())) return;
  String message = "File Not Detected\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " NAME:"+server.argName(i) + "\n VALUE:" + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  Serial.println(message);
}

bool loadFromSpiffs(String path){
  String dataType = "text/plain";
  if(path.endsWith("/")) path += "index.htm";

  if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
  else if(path.endsWith(".html")) dataType = "text/html";
  else if(path.endsWith(".htm")) dataType = "text/html";
  else if(path.endsWith(".css")) dataType = "text/css";
  else if(path.endsWith(".js")) dataType = "application/javascript";
  else if(path.endsWith(".png")) dataType = "image/png";
  else if(path.endsWith(".gif")) dataType = "image/gif";
  else if(path.endsWith(".jpg")) dataType = "image/jpeg";
  else if(path.endsWith(".ico")) dataType = "image/x-icon";
  else if(path.endsWith(".xml")) dataType = "text/xml";
  else if(path.endsWith(".pdf")) dataType = "application/pdf";
  else if(path.endsWith(".zip")) dataType = "application/zip";
  File dataFile = SPIFFS.open(path.c_str(), "r");
  if (server.hasArg("download")) dataType = "application/octet-stream";
  if (server.streamFile(dataFile, dataType) != dataFile.size()) {
  }

  dataFile.close();
  return true;
}


// --- FUNCOES AUXILIARES

void conectarWifi() {
  //Connect to wifi Network
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
}

void iniciarSensores() {
  int valor_s1 = 0, valor_s2 = 0, valor_s3 = 0;

  //Define os pinos dos sensores
  pinMode(sensor1, INPUT);
  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);

  // define os pinos dos les indicadores
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);  
}

String lerSensores() {
  //Leitura dos sensores
  int valor_s1 = digitalRead(sensor1);
  int valor_s2 = digitalRead(sensor2);
  int valor_s3 = digitalRead(sensor3);

  // TODO retirar esta linha abaixo quando o sensor tiver instalado
  valor_s3 = 0;

  String percent_minimo = "25";
  String percent_medio = "65";
  String percent_maximo = "90";

  String json = "";
  String status_geral = "normal";
  String sensores = "";
  String sensores_status = "normal";
  String nivel = "";
  String nivel_status = "";
  int nivel_num = 10;

  //Checa o nivel
  if (valor_s1 == 1) {
    digitalWrite(led1, HIGH);
    nivel_num = 35;
    status_geral = "alerta";
    nivel_status = "abaixo da metade";
    nivel = "O nível de água está entre 20% e 40%";
    sensores = "<li>* - Ligado</li>";
  }
  else {
    digitalWrite(led1, LOW);
    status_geral = "perigo";
    nivel_status = "muito baixo";
    nivel = "O nível de água está abaixo de 20%";
    sensores = "<li>- - Desligado</li>";
  }

  if (valor_s2 == 1) {
    digitalWrite(led2, HIGH);
    nivel_num = 65;
    status_geral = "normal";
    nivel_status = "acima da metade";
    nivel = "O nível de água está entre 60% e 80%";
    sensores = "<li>* - Ligado</li>" + sensores;
    if (valor_s1 == 0) {
      sensores_status = "Há um erro no sensor 1";
      status_geral = "aviso";
    }
  }
  else {
    digitalWrite(led2, LOW);
    sensores = "<li>- - Desligado</li>" + sensores;
  }

  if (valor_s3 == 1) {
    digitalWrite(led3, HIGH);
    nivel_num = 90;
    status_geral = "perigo";
    nivel_status = "muito acima";
    nivel = "O nível de água está acima de " + percent_maximo + "%";
    sensores = "<li>* - Ligado</li>" + sensores;
    if (valor_s1 == 0 && valor_s2 == 0) {
      sensores_status = "Há problemas nos sensores 1 e 2";
    }
    else {
      if (valor_s2 == 0) {
        sensores_status = "Há um erro no sensor 2";
      }
      
      if (valor_s1 == 0) {
        sensores_status = "Há um erro no sensor 1";  
      }
    }
  }
  else {
    digitalWrite(led3, LOW);
    sensores = "<li>- - Desligado</li>" + sensores;
  }

  json = "{\"sensores\": \"" + sensores;
  json = json + "\", \"sensores_status\":\"" + sensores_status;
  json = json + "\", \"status_geral\":\"" + status_geral;
  json = json + "\", \"nivel_num\":" + String(nivel_num);
  json = json + ", \"nivel\":\"" + nivel;
  json = json + "\", \"nivel_status\":\"" + nivel_status + "\"}";

  return json;
}
