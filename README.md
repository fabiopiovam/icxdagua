# icxdagua - Caixa d'água inteligente

Protótipo de solução para controle de nível da caixa d'água utilizando [ESP8266 - NodeMCU](https://github.com/esp8266/Arduino), `sensores` e `LEDs`.

# Inspiração

Inspirado no curso Artesino realizado em 07/2019 no SESC Santos por Antonio Braga
